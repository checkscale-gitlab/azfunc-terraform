Quick demo of Terraform and Azure Devops Pipelines to deploy Azure resources

Key examples:
- Use of Terraform modules `terraform/modules`
- Import process example `terraform/app/import-app-service`
- azure pipeline `pipelines/terraform.yml`, incl input parameter for self-service/dynamic deployments
- azure pipeline template `pipelines/templates/tf-all.yml` which:
  - Uses Terraform CLI, because the official pipeline tasks are pretty much abandoned
  - Evaluates Terraform plan actions, allows you to toggle a pipeline error if plan is destructive
  - Pushes Terraform Outputs to Azure App Configuration service as key pairs
- Protect resources `terraform/modules/ms-sql`
  - [Azure Locks](https://docs.microsoft.com/en-us/azure/azure-resource-manager/management/lock-resources) protect the resource in Azure
  - Terraform lifecycle controls applied to the Azure Locks (+ resources) reduce risk that Terraform will destroy resources
    - Closest equivalent of CloudFormation DeletionPolicy: Retain
    - Terraform resource lifecycle controls don't prevent resource deletion when a resource is removed from the terraform configuration
  - Probably best to manage Azure Locks outside of Terraform or in a separate config
  - Issues discussed in [open git issue](https://github.com/hashicorp/terraform/issues/17599)
- Use of data objects to use resources which are not defined in the current terraform config `terraform/app/app-service-0` - uses App Service Plan managed by `terraform/shared`
- [Terratest](https://github.com/gruntwork-io/terratest) quick integration testexample `terraform/modules/azure-function/test`

# TODO
- Investigate use of terraform supporting tools:

    - [tfsec](https://github.com/tfsec/tfsec) - "analysis of your terraform templates to spot potential security issues"
- Investigate appropriate functional test. Aim: Quickly verify that the deployment has finished. Rather than full functional test of app
    - Could be used to inform a rollback function for failed deployments

# Notes
- az pipeline Terraform tasks have been [abandoned](https://github.com/microsoft/azure-pipelines-extensions/pull/889)
    - [MS dev blog on using tf CLI instead](https://cloudarchitected.com/2021/02/using-terraform-in-azure-pipelines-without-extensions/)