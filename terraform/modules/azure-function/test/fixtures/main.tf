terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>2.65"
    }
  }
  required_version = ">= 1.0.0"
}

provider "azurerm" {
  features {}
}

variable "group_name" {
  type = string
}

variable "location" {
  type = string
}

variable "stage" {
  type = string
}

# Put below in outputs.tf
output "primary_name" {
  value = module.primary.azfunc_name
}

output "primary_url" {
  value = module.primary.azfunc_url
}

# Pending https://github.com/terraform-providers/terraform-provider-azurerm/pull/12482
#output "secondary_name" {
#  value = module.secondary.azfunc_name
#}
#
#output "secondary_url" {
#  value = module.secondary.azfunc_url
#}

# Start of main.tf
resource "random_id" "id" {
  byte_length = 4
}

resource "azurerm_resource_group" "this" {
  name     = var.group_name
  location = var.location
}

resource "azurerm_storage_account" "this" {
  name                      = "${var.stage}${random_id.id.hex}"
  resource_group_name       = azurerm_resource_group.this.name
  location                  = var.location
  account_tier              = "Standard"
  account_replication_type  = "LRS"
  enable_https_traffic_only = true
}

module "primary" {
 #source = "git::<repo-url>?ref=<tag>"
  source = "../.." #azure-function

  stage               = var.stage
  location            = var.location
  function_name       = "primary"
  os                  = "linux"
  resource_group_name = azurerm_resource_group.this.name
  storage_account = {
    name = "${var.stage}${random_id.id.hex}"
    key  = azurerm_storage_account.this.primary_access_key
  }
  app_service_plan_config = {
    kind = "FunctionApp",
    tier = "Dynamic",
    size = "Y1"
  }
}

# Pending https://github.com/terraform-providers/terraform-provider-azurerm/pull/12482
#module "secondary" {
# #source = "git::<repo-url>?ref=<tag>"
#  source = "../" #azure-function
#
#  stage               = var.stage
#  location            = var.location
#  function_name       = "secondary"
#  os                  = "windows"
#  resource_group_name = azurerm_resource_group.this.name
#  storage_account = {
#    name = "${var.stage}${random_id.id.hex}"
#    key  = azurerm_storage_account.this.primary_access_key
#  }
#  app_service_plan_config = {
#    kind = "FunctionApp",
#    tier = "Dynamic",
#    size = "Y1"
#  }
#}