data "azuread_users" "readers" {
  user_principal_names = var.readers
}

data "azuread_users" "owners" {
  user_principal_names = var.owners
}

resource "azuread_group" "readers" {
  display_name            = "${var.project_name}_readers"
  prevent_duplicate_names = true
  members                 = data.azuread_users.readers.object_ids
}

resource "azuread_group" "owners" {
  display_name            = "${var.project_name}_owners"
  prevent_duplicate_names = true
  members                 = data.azuread_users.owners.object_ids
}

resource "azurerm_role_assignment" "readers" {
  scope                = azurerm_resource_group.this.id
  role_definition_name = "Reader"
  principal_id         = azuread_group.readers.id
}

resource "azurerm_role_assignment" "owners" {
  scope                = azurerm_resource_group.this.id
  role_definition_name = "Owner"
  principal_id         = azuread_group.owners.id
}